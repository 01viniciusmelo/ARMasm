#include <jni.h>
#include <string>
#include <iostream>

using namespace std;
extern "C"
JNIEXPORT void JNICALL
Java_ru_dk_armasm_MainActivity_runASMCodeFromString(JNIEnv *env, jobject instance,
                                                    jstring string_) {
    const char *string_p = env->GetStringUTFChars(string_, 0);
    string str(string_p);

//    asm(str);

    env->ReleaseStringUTFChars(string_, str.data());
}

extern "C"
jint
Java_ru_dk_armasm_MainActivity_intFromJNI(JNIEnv *env, jobject instance) {
    int a = 10, b = 5, res = -5;
    asm("add %0, r0 " : "=r" (res) : "r" (a), "r"(b));

    return res;

}

extern "C"
jstring
Java_ru_dk_armasm_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
